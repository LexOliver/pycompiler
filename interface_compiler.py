import sys
from PyQt4 import QtGui

class Window(QtGui.QMainWindow):
  def __init__(self):
    super(Window, self).__init__()
    self.setGeometry(50, 50, 500, 300)
    self.setWindowTitle("PyCompiler")

    self.show()

app = QtGui.QApplication(sys.argv)
GUI = Window()
sys.exit(app.exec_())

# window = QtGui.QWidget()
# window.setGeometry(0, 0, 500, 300)
# window.setWindowTitle("PyCompiler")
# window.show()
