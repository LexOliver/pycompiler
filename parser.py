import ply.yacc as yacc

# Get the token map from the lexer (REQUIRED)
from scanner import tokens

# CONTEXT-FREE GRAMMAR ESPECIFICATION REFERRING TO THE LANGUAGE "PLOPS"

# -- Precedence
precedence = (
  ('left', 'LOGIC_ARITMETIC_OR', 'LOGIC_ARITMETIC_AND'),
  ('nonassoc', 'RELATIONAL_OP'),
  ('left', 'PLUS', 'MINUS'),
  ('left', 'TIMES', 'DIVIDE', 'MODULUS'),
  ('right', 'LOGIC_ARITMETIC_NOT')
)

# -- Start symbol
def p_programa(p):
  """
    programa : decl_global
             | programa decl_global
  """
  p[0] = ('Símbolo-inicial', p[1])

def p_decl_global(p):
  """
  decl_global : decl_variavel
              | decl_funcao
  """
  p[0] = ('Declaração-global', p[1])

def p_decl_variavel(p):
  """
  decl_variavel : VAR lista_idents MINUS tipo SEMICOLON
  """
  p[0] = (p[4], p[2])
  
def p_decl_variavel_error(p):
  """
  decl_variavel : VAR error MINUS tipo SEMICOLON
  """
  print(f'Bad variable initialization at line {p.lineno(2)}, column {p.lexpos(2)}')

def p_lista_idents(p):
  """
  lista_idents : IDENTIFIER
               | lista_idents COMMA IDENTIFIER 
  """
  if(len(p) == 2):
    p[0] = ('Identificador', p[1])
  elif(len(p) == 4):
    p[0] = ('Identificador', p[3])

def p_tipo(p):
  """
  tipo : INT
       | TYPE_CHAR
       | TYPE_FLOAT
  """
  p[0] = (p[1],)

def p_decl_funcao(p):
  """
  decl_funcao : PROC nome_args MINUS tipo bloco
              | PROC nome_args bloco
  """
  if(len(p) == 6):
    p[0] = ('Declaração-função', p[2], p[4], p[5])
  elif(len(p) == 4):
    p[0] = ('Declaração-função', p[2], 'void', p[3])

# Corrigir abaixo!
def p_nome_args(p):
  """
  nome_args : IDENTIFIER LPAREN param_formais RPAREN
            | nome_args IDENTIFIER LPAREN param_formais RPAREN
  """
  if(len(p) == 5):
    p[0] = ('Argumento', p[1], p[3])
  elif(len(p) == 6):
    p[0] = ('Argumento', p[2], p[4])

def p_nome_args_error(p):
  """
  nome_args : IDENTIFIER LPAREN error RPAREN
            | nome_args IDENTIFIER LPAREN error RPAREN
  """
  if(len(p) == 5):
    print(f'Syntax error in argument definitions at line {p.lineno(1)}, column {p.lexpos(1)}')
  elif(len(p) == 6):
    print(f'Syntax error in argument definitions at line {p.lineno(2)}, column {p.lexpos(2)}')

# Corrigir abaixo!
def p_param_formais(p):
  """
  param_formais : IDENTIFIER MINUS tipo
                | param_formais COMMA IDENTIFIER MINUS tipo
                | empty
  """
  if(len(p) == 4):
    p[0] = (p[3], p[1])
  elif(len(p) == 6):
    p[0] = (p[5], p[3])
  elif(len(p) == 2):
    p[0] = (p[1],)

def p_bloco(p):
  """
  bloco : LBRACKET lista_comandos RBRACKET
  """
  p[0] = ('Bloco-de-comandos', p[2])

def p_bloco_error(p):
  """
  bloco : LBRACKET error RBRACKET
  """
  print(f'Syntax error inside block at line {p.lineno(2)}')

# Corrigir abaixo!
def p_lista_comandos(p):
  """
  lista_comandos : comando
                 | lista_comandos comando
                 | empty
  """
  if(len(p) == 2):
    p[0] = (p[1],)
  elif(len(p) == 3):
    p[0] = (p[2],)

def p_comando(p):
  """
  comando : decl_variavel
          | atribuicao
          | iteracao
          | decisao
          | escrita
          | retorno
          | bloco
          | chamada_func_cmd
  """
  p[0] = (p[1],)

def p_comando_error(p):
  """
  comando : error
  """
  print(f'Syntax error at line {p.lineno(1)}, column {p.lexpos(1)}')

def p_atribuicao(p):
  """
  atribuicao : IDENTIFIER ASSIGNMENT_OP expressao SEMICOLON
  """
  p[0] = (p[2], p[1], p[3])

def p_atribuicao_error(p):
  """
  atribuicao : IDENTIFIER ASSIGNMENT_OP error SEMICOLON
  """
  print(f'Syntax error in assignment at line {p.lineno(3)}')

def p_iteracao(p):
  """
  iteracao : WHILE LPAREN expressao RPAREN comando
  """
  p[0] = (p[1], p[3], p[5])

def p_iteracao_error(p):
  """
  iteracao : WHILE LPAREN error RPAREN comando
  """
  print(f'Syntax error in while condition at line {p.lineno(3)}, column {p.lexpos(3)}')

def p_decisao(p):
  """
  decisao : IF LPAREN expressao RPAREN comando ELSE comando
          | IF LPAREN expressao RPAREN comando
  """
  if(len(p) == 8):
    p[0] = ('If-then-else', p[3], p[5], p[7])
  elif(len(p) == 6):
    p[0] = ('If-then', p[3], p[5])

def p_escrita(p):
  """
  escrita : PRINT LPAREN lista_exprs RPAREN SEMICOLON
  """
  p[0] = (p[1], p[3])

def p_chamada_func_cmd(p):
  """
  chamada_func_cmd : chamada_func SEMICOLON
  """
  p[0] = (p[1],)

def p_retorno(p):
  """
  retorno : RETURN expressao SEMICOLON
  """
  p[0] = (p[1], p[2])

# Corrigir abaixo!
def p_chamada_func(p):
  """
  chamada_func : IDENTIFIER LPAREN lista_exprs RPAREN
               | chamada_func IDENTIFIER LPAREN lista_exprs RPAREN
  """
  if(len(p) == 5):
    p[0] = (p[1], p[3])
  elif(len(p) == 6):
    p[0] = (p[2], p[4])

# Corrigir abaixo!
def p_lista_exprs(p):
  """
  lista_exprs : empty
              | expressao
              | lista_exprs COMMA expressao
  """
  if(len(p) == 2):
    p[0] = (p[1],)
  elif(len(p) == 4):
    p[0] = (p[3],)

# Corrigir abaixo!
def p_expressao(p):
  """
  expressao : expressao RELATIONAL_OP expressao
            | expressao LOGIC_ARITMETIC_AND expressao
            | expressao LOGIC_ARITMETIC_OR expressao
            | expressao PLUS expressao
            | expressao MINUS expressao
            | expressao TIMES expressao
            | expressao DIVIDE expressao
            | expressao MODULUS expressao
            | expr_basica
  """
  if(len(p) == 4):
    p[0] = (p[2], p[1], p[3])
  elif(len(p) == 2):
    p[0] = (p[1],)

# Corrigir abaixo!
def p_expr_basica(p):
  """
  expr_basica : LPAREN expressao RPAREN
              | LOGIC_ARITMETIC_NOT expr_basica
              | INTEGER
              | CHAR
              | FLOAT
              | IDENTIFIER
              | chamada_func
  """
  if(len(p) == 4):
    p[0] = (p[1], p[2], p[3])
  elif(len(p) == 3):
    p[0] = (p[1], p[2])
  elif(len(p) == 2):
    p[0] = (p[1],)

def p_empty(p):
  """
  empty :
  """
  pass

# -- Syntax error handling
def p_error(p):
  print(f"Syntax error at line {p.lineno(0)}, {p.lexpos(0)}!")

# PARSER CONSTRUCTOR...
parser = yacc.yacc(start='programa')

# TEST: Evaluating structure.
while True:
  try:
    # Input deve ser feito na interface!!
    s = input('PLOPS >')
  except EOFError:
    break
  if not s: continue
  # PARSING INPUT
  result = parser.parse(s)
  print(result)
