import ply.lex as lex

# REGULAR EXPRESSIONS REFERRING TO THE LANGUAGE "PLOPS"
# -- Reserved words
reserved = {
  'if': 'IF',
  'else': 'ELSE',
  'while': 'WHILE',
  'return': 'RETURN',
  'float': 'TYPE_FLOAT',
  'char': 'TYPE_CHAR',
  'void': 'VOID',
  'prnt': 'PRINT',
  'int': 'INT',
  'and': 'LOGIC_ARITMETIC_AND',
  'or': 'LOGIC_ARITMETIC_OR',
  'not': 'LOGIC_ARITMETIC_NOT',
  'proc': 'PROC',
  'var': 'VAR'
}
# -- TOKENS
tokens = [
  'IDENTIFIER',
  'RELATIONAL_OP',
  'PLUS',
  'MINUS',
  'TIMES',
  'DIVIDE',
  'MODULUS',
  'ASSIGNMENT_OP',
  'SEMICOLON',
  'LPAREN',
  'RPAREN',
  'COMMA',
  'LBRACKET',
  'RBRACKET',
  'INTEGER',
  'FLOAT',
  'TYPE_FLOAT',
  'CHAR',
  'TYPE_CHAR'
] + list(reserved.values())

# -- Simple regexes
t_RELATIONAL_OP = r'<=|>=|<>|<|>|='
t_PLUS = r'\+'
t_MINUS = r'\-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MODULUS = r'%'
t_ASSIGNMENT_OP = r':\='
t_SEMICOLON = r';'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_COMMA = r','
t_LBRACKET = r'{'
t_RBRACKET = r'}'
t_ignore = r' '

def t_COMMENT(t):
  r'(\*\*(.)*?\n)|(>>(.|\n)*?<<)'
  pass

def t_IDENTIFIER(t):
  r'[a-z]([a-zA-Z_0-9]*)'
  t.type = reserved.get(t.value, 'IDENTIFIER')
  return t

def t_FLOAT(t):
  r'(\-|\+)?\d+(,\d+)?[Ee](\-|\+)?\d+(,\d+)?|\d*,\d+|(\-|\+)?\d+,\d*'
  
  tmp = t.value.lower().split('e')
  if ',' in tmp[1]:
    t.value = float(tmp[0].replace(',', '.')) * 10 ** float(tmp[1].replace(',', '.'))
  else:
    t.value = float(t.value.replace(',', '.'))
  del tmp
  return t

def t_INTEGER(t):
  r'(\-|\+)?\d+'
  t.value = int(t.value)
  return t

def t_CHAR(t):
  r"'([0-9]|[a-zA-Z]|\n|\t|\s|:|\(|\)|,)'"
  t.type = 'CHAR'
  return t

# -- Lexic error handling
def t_error(t):
  print(f"Illegal characters '{t.value}' at line {t.lineno}, column {t.lexpos}!")
  t.lexer.skip(len(t.value))

# -- New line handling
def t_newline(t):
  r'\n+'
  t.lexer.lineno += len(t.value)

# LEXER CONSTRUCTOR...
lexer = lex.lex()

# INPUT.... Deve ser recebida na interface e avaliada pelo lexer e retornar os tokens
# input_file = """
# proc main()
# {
#   var x, y - int;
#   var m - int;

#   x := 120;   >> dois valores a partir dos quais <<
#   y := 640;   >> sera calculado o m.d.c.         <<

#   prnt('m', 'd', 'c', '(');
#   prnt( x );
#   prnt(',');
#   prnt( y );
#   prnt(')', ':');
#   prnt( m );
# }

# proc mdcDe(x - int) e (y - int) - int
# {
#   if (y = 0) {
#     return x;
#   } else {
#     return mdcDe(y) e (x % y);
#   }

# }
# """

# input_file = "50 - 40 >>blabslasbdlabdla \nsfahslasf \n asfasfaf<<"
# Na construção do LEXER pela interface, DEVE SER PASSADO A ENTRADA!
# lexer.input(input_file)

# TEST: Evaluating tokens.
# while True:
#   tok = lexer.token()
#   if not tok:
#     break
#   print(tok)